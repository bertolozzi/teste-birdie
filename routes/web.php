<?php

use App\Task;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $request) {
    $task = new Task;

    if (!empty($request->search)) {
        $search = $request->search;
        $task->where(function ($q) use ($search) {
            $q->orWhere(
                'title',
                'like',
                "%{$search}%"
            )->orWhere(
                'description',
                'like',
                "%{$search}%"
            );
        })->get();
    } else {
        $search = '';
    }

    $pending = $task->where('status', 'pending')->get();
    $inProgress = $task->where('status', 'in-progress')->get();
    $finished = $task->where('status', 'finished')->get();

    return view('site', compact('pending', 'inProgress', 'finished', 'search'));
})->name('site');

Route::get('/task/{id}/{title?}', function ($id) {
    $task = Task::find($id);

    if ($task) {
        return view('task', compact('task'));
    }
    
    return redirect()->route('site');
})->name('task');

Auth::routes();

Route::get('/admin', 'AdminController@index')->name('admin')->middleware('can:isExecutor');

Route::middleware([ 'auth' ])->prefix('admin')->namespace('Admin')->group(function () {
    
    Route::resource('tasks', 'TaskController')->middleware('can:isExecutor');
    Route::resource('users', 'UserController')->middleware('can:isAdmin');
    Route::resource('executors', 'ExecutorController')->middleware('can:isAdmin');
    Route::resource('admins', 'AdminController')->middleware('can:isAdmin');
});
