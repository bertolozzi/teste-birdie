<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = json_encode([
            [
                'title' => 'Admin',
                'url' => ''
            ]
        ]);

        $totals = [
            'tasks' => Task::count(),
            'users' => User::count(),
            'executors' => User::where('role', 'executor')->count(),
            'admins' => User::where('role', 'admin')->count(),
        ];

        return view('admin', compact('breadcrumbs', 'totals'));
    }
}
