# Birdie ToDo

Projeto criado como teste para a Birdie.

## Configuração

Na pasta do projeto:

- Execute o comando `composer install`.
- Copie e renomeie o arquivo `.env.example` para `.env`.
- Execute o comando `php artisan key:generate`.
- Crie um arquivo chamado `database.sqlite` dentro do diretório `database`.
- Execute o comando `php artisan migrate --seed`.
- Execute o comadno `php artisan serve`.

## Acesso

Após os passos de configuração acesse http://localhost:8000, será criado um usuário admin padrão:

```
e-mail: admin@birdie.ai
senha: admin
```
