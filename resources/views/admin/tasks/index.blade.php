@extends('layouts.app')

@section('content')
<vue-page size="12">
    @if ($errors->all())
        <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            @foreach ($errors->all() as $error)
                <p><strong>{{ $error }}</strong></p>
            @endforeach
        </div>
    @endif

    <vue-panel title="Task List">
        <vue-breadcrumbs :links="{{$breadcrumbs}}"></vue-breadcrumbs>
        
        <vue-list-table
            :titles="[ '#', 'Title', 'Description', 'Status', 'Ending' ]"
            :items="{{ json_encode($tasks) }}"
            :modal="true"
            sort="desc"
            sort-col="0"
            create-url="#create"
            show-url="/admin/tasks/"
            edit-url="/admin/tasks/"
            delete-url="/admin/tasks/"
            csrf-toekn="{{ csrf_token() }}">
        </vue-list-table>

        <div align="center">
            {{ $tasks }}
        </div>
    </vue-panel>
</vue-page>

<vue-modal-content id="createModal" title="Create">
    <vue-form id="createForm" css="" action="{{ route('tasks.store') }}" method="post" enctype="" csrf-token="{{ csrf_token() }}">
        <div class="form-group">
            <label for="title">Title</label>
            <input id="title" name="title" type="text" class="form-control" placeholder="Title" value="{{ old('title') }}">
        </div>
        
        <div class="form-group">
            <label for="description">Description</label>
            <input id="description" name="description" type="text" class="form-control" placeholder="Description" value="{{ old('description') }}">
        </div>

        <div class="form-group">
            <label for="status">Status</label>
            <select name="status" id="status" class="form-control">
                <option {{ old('status') && old('status') == 'pending' ? 'selected' : '' }} value="pending">Pending</option>
                <option {{ old('status') && old('status') == 'in-progress' ? 'selected' : '' }} value="in-progress">In Progress</option>
                <option {{ old('status') && old('status') == 'finished' ? 'selected' : '' }} value="finished">Finished</option>
            </select>
        </div>

        <div class="form-group">
            <label for="executors">Executors</label>
            <vue-select name="executors[]" id="executors" class="" multiple :options="{{ json_encode($users) }}" :settings="{ text: name }">
                @foreach ($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </vue-select>
        </div>

        <div class="form-group">
            <label for="ending_date">Ending</label>
            <input id="ending_date" name="ending_date" type="date" class="form-control" value="{{ old('ending_date') }}">
        </div>
    </vue-form>

    <span slot="footer">
        <button form="createForm" class="btn btn-info">Add</button>
    </span>
</vue-modal-content>

<vue-modal-content id="showModal" :title="$store.state.item.title">
    <div class="row">
        <div class="col-md-2">
            <b>Description:</b>
        </div>
        <div class="col-md-10">
            <p>@{{$store.state.item.description}}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <b>Executors:</b>
        </div>
        <div class="col-md-10">
            <vue-list-array :array="$store.state.item.users"></vue-list-array>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <b>Status:</b>
        </div>
        <div class="col-md-10">
            <p>@{{$store.state.item.status}}</p>
        </div>
    </div>
</vue-modal-content>

<vue-modal-content id="editModal" title="Edit">
    <vue-form id="editForm" css="" :action="`/admin/tasks/${$store.state.item.id}`" method="put" enctype="" csrf-token="{{ csrf_token() }}">
        <div class="form-group">
            <label for="title">Title</label>
            <input id="title" name="title" type="text" class="form-control" v-model="$store.state.item.title" placeholder="Title">
        </div>
        
        <div class="form-group">
            <label for="description">Description</label>
            <input id="description" name="description" type="text" class="form-control" v-model="$store.state.item.description" placeholder="Description">
        </div>
        
        <div class="form-group">
            <label for="status">Status</label>
            <select name="status" id="status" class="form-control" v-model="$store.state.item.status">
                <option value="pending">Pending</option>
                <option value="in-progress">In Progress</option>
                <option value="finished">Finished</option>
            </select>
        </div>
    
        <div class="form-group">
            <label for="executors">Executors</label>
            <vue-select name="executors[]" id="executors" class="form-control" multiple style="width: 100%" v-model="$store.state.item.executors">
                @foreach ($users as $user)                    
                <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </vue-select>
        </div>
    
        <div class="form-group">
            <label for="ending_date">Ending</label>
            <input id="ending_date" name="ending_date" type="date" class="form-control" v-model="$store.state.item.ending_date">
        </div>
    </vue-form>

    <span slot="footer">
        <button form="editForm" class="btn btn-info">Update</button>
    </span>
</vue-modal-content>
@endsection
