@extends('layouts.app')

@section('content')
<vue-page size="12">
    <vue-panel title="Tasks">

        <p>
            <form action="{{ route('site') }}" class="form-inline text-center" method="get">
                <input type="search" class="form-control" name="search" placeholder="Search..." value="{{ empty($search) ? '' : $search }}">
                <button class="btn btn-info">Search</button>
            </form>
        </p>

        <div class="row">
            <div class="col-md-4">
                <vue-panel title="Pending">
                    <div class="row">
                        @foreach ($pending as $task)
                            <vue-task-card
                                link="{{ route('task', [ $task->id, str_slug($task->title) ]) }}"
                                ending-date="{{ $task->ending_date }}"
                                executors="{{ $task->executors }}"
                                title="{{ str_limit($task->title, 25, '...') }}"
                                description="{{ str_limit($task->description, 40, '...') }}">
                            </vue-task-card>
                        @endforeach
                    </div>
                </vue-panel>
            </div>

            <div class="col-md-4">
                <vue-panel title="In Progress">
                    <div class="row">
                        @foreach ($inProgress as $task)
                            <vue-task-card
                                link="{{ route('task', [ $task->id, str_slug($task->title) ]) }}"
                                ending-date="{{ $task->ending_date }}"
                                executors="{{ $task->executors }}"
                                title="{{ str_limit($task->title, 25, '...') }}"
                                description="{{ str_limit($task->description, 40, '...') }}">
                            </vue-task-card>
                        @endforeach
                    </div>
                </vue-panel>
            </div>

            <div class="col-md-4">
                <vue-panel title="Finished">
                    <div class="row">
                        @foreach ($finished as $task)
                            <vue-task-card
                                link="{{ route('task', [ $task->id, str_slug($task->title) ]) }}"
                                ending-date="{{ $task->ending_date }}"
                                executors="{{ $task->executors }}"
                                title="{{ str_limit($task->title, 25, '...') }}"
                                description="{{ str_limit($task->description, 40, '...') }}">
                            </vue-task-card>
                        @endforeach
                    </div>
                </vue-panel>
            </div>
        </div>
    </vue-panel>
</vue-page>
@endsection
