@extends('layouts.app')

@section('content')
<vue-page size="12">
    <vue-panel>
        <h2 align="center">{{ $task->title }}</h2>

        <h4 align="center">{{ $task->description }}</h4>

        <p>
            {!! $task->content !!}
        </p>

        <p align="center">
            <small>Executors: {{ $task->exedcutors }} - {{ date('d/m/Y', strtotime($task->publish_datetime)) }}</small>
        </p>
    </vue-panel>
</vue-page>
@endsection
