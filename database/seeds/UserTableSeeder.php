<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = App\User::where('email', 'admin@birdie.ai')->first();

        if (!$admin) {
            // Criando o usuário admin
            $admin = new App\User([
                'name' => 'Administrador',
                'email' => 'admin@birdie.ai',
                'role' => 'admin',
                'password' => bcrypt('admin')
            ]);

            $admin->save();
        }
    }
}
